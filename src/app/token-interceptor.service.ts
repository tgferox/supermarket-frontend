import { Injectable, Injector} from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { WebService } from './web.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(public injector: Injector) { }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let webService = this.injector.get(WebService)
    let tokenizedReq = req.clone({
      headers: req.headers.set(
        'x-access-token', `${webService.getToken()}`
      )
    });
    return next.handle(tokenizedReq)
  }
}
