import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: any;

  constructor(private webService: WebService, private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
    
  isInvalid(control: any) {
    return this.loginForm.controls[control].invalid && this.loginForm.controls[control].touched;
  }

  isUntouched() {
    return this.loginForm.controls.username.pristine || this.loginForm.controls.password.pristine;
    }

  isIncomplete() {
  return this.isInvalid('username') || this.isInvalid('password') || this.isUntouched();
  }

  onSubmit(){
    this.webService.login(this.loginForm.value)
    .subscribe({
      next: data => {
        let token = Object.values(data)
        localStorage.setItem('token', token[0])
        this.router.navigate([''])
      },
      error: error => {
        this.login_message = 'Error: ' + error.error["error"]
      }
    });
  }

 
  login_message: any = "";
}
