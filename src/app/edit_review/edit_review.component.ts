import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit_review',
  templateUrl: './edit_review.component.html',
  styleUrls: ['./edit_review.component.css']
})

export class EditReviewComponent implements OnInit {

  reviewForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.reviewForm = this.formBuilder.group({
      comment: ['', Validators.required],
      rating: ['', Validators.required]
    });
    this.webService.getReview(this.route.snapshot.params['product_id'], this.route.snapshot.params['review_id'])
    .subscribe({
      next: data =>{
        let dataValues = data
        this.review = dataValues
        this.webService.checkUser(this.review['user'])
        .subscribe({
          next: data => {  
          },
          error: err=> {
            this.router.navigate(['/product/' + this.route.snapshot.params['product_id'] + '/reviews' + this.route.snapshot.params['review_id']])
          }
        })
      },
      error: error =>{
        this.router.navigate([''])
      }
    });
  }
    
  isInvalid(control: any) {
    return this.reviewForm.controls[control].invalid && this.reviewForm.controls[control].touched;
  }

  isIncomplete() {
  return this.isInvalid('comment') || this.isInvalid('rating');
  }

  onSubmit(){
    this.webService.putReview(this.reviewForm.value, this.route.snapshot.params['product_id'], this.route.snapshot.params['review_id'])
    .subscribe({
      next: data => {
        let uri = Object.values(data)
        this.router.navigate([uri[0]])
      },
      error: error => {
        this.review_message = 'Error: ' + error.error["error"]
      }
    });
  }

  onDelete(){
    this.webService.deleteReview(this.route.snapshot.params['product_id'], this.route.snapshot.params['review_id'])
    .subscribe({
      next: data=>{
        this.router.navigate([''])
      },
      error: error=> {
        this.delete_message = 'Error: ' + error.error["error"]
      }
    });
  }

  review: any = {"comment": "", "rating": ""};
  review_message: any = "";
  delete_message: any = "";
}

