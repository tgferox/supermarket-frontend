import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { NewUserComponent } from './new_user/new_user.component';
import { EditUserComponent } from './edit_user/edit_user.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { WebService } from './web.service';
import { HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { EditProductComponent } from './edit_product/edit_product.component';
import { NewProductComponent } from './new_product/new_product.component';
import { NewReviewComponent } from './new_review/new_review.component';
import { EditReviewComponent } from './edit_review/edit_review.component';
import { ReviewComponent } from './review/review.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    HomeComponent,
    ProductComponent,
    NewProductComponent,
    NewReviewComponent,
    EditProductComponent,
    NewUserComponent,
    EditUserComponent,
    LoginComponent,
    RegisterComponent,
    EditReviewComponent,
    ReviewComponent,
    UserComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [WebService, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
