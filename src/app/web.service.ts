import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  getToken(){
    return localStorage.getItem('token')
  }
  constructor(private http: HttpClient) { }

  getProducts(page: number, page_size: number) {
    return this.http.get(
      'http://localhost:5000/api/v1/products/?page_num=' + page + '&page_size=' + page_size);
  }

  getProduct(id: number) {
    return this.http.get(
      'http://localhost:5000/api/v1/products/' + id + '/');
    }

  getReviews(page: number, page_size: number, id: number) {
    return this.http.get(
      'http://localhost:5000/api/v1/products/' + id + '/reviews/?page_num=' + page + '&page_size=' + page_size);
  }

  getReview(product_id: number, review_id: number) {
    return this.http.get(
      'http://localhost:5000/api/v1/products/' + product_id + '/reviews/' + review_id + '/');
    }

  getUser(id: number) {
    console.log("ello");
    return this.http.get('http://localhost:5000/api/v1/users/' + id + '/');
  }

  getUsers(page: number, page_size: number) {
    return this.http.get(
      'http://localhost:5000/api/v1/users/?page_num=' + page + '&page_size=' + page_size);
  }

  postProduct(productForm: any) {
    let postData = new FormData();
    postData.append("title", productForm.title);
    postData.append("description", productForm.description);
    postData.append("price", productForm.price);
    postData.append("type", productForm.type);

    return this.http.post('http://localhost:5000/api/v1/products/', postData)
  }

  postReview(reviewForm: any, id: number) {
    let postData = new FormData();
    postData.append("comment", reviewForm.comment);
    postData.append("rating", reviewForm.rating);

    return this.http.post('http://localhost:5000/api/v1/products/' + id + '/reviews/', postData)
  }

  postUser(userForm: any) {
    let postData = new FormData();
    postData.append("username", userForm.username);
    postData.append("email", userForm.email);
    postData.append("password", userForm.password);
    postData.append("password_confirm", userForm.password_confirm);
    postData.append("is_admin", userForm.is_admin);

    return this.http.post('http://localhost:5000/api/v1/users/', postData)
  }

  register(registerForm: any) {
    let postData = new FormData();
    postData.append("username", registerForm.username);
    postData.append("email", registerForm.email);
    postData.append("password", registerForm.password);
    postData.append("password_confirm", registerForm.password_confirm);

    return this.http.post('http://localhost:5000/api/v1/register/', postData)
  }

  login(loginForm: any){
    let username = loginForm.username
    let password  = loginForm.password
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(username + ':' + password)
      })
    }
    return this.http.post('http://localhost:5000/api/v1/login/', '',httpOptions)
  }

  putProduct(productForm: any, id: number) {
    let postData = new FormData();
    postData.append("title", productForm.title);
    postData.append("description", productForm.description);
    postData.append("price", productForm.price);
    postData.append("type", productForm.type);

    return this.http.put('http://localhost:5000/api/v1/products/' + id + '/', postData)
  }

  putReview(reviewForm: any, product_id: number, review_id: number) {
    let postData = new FormData();
    console.log(reviewForm.comment)
    postData.append("comment", reviewForm.comment);
    postData.append("rating", reviewForm.rating);

    return this.http.put('http://localhost:5000/api/v1/products/' + product_id + '/reviews/' + review_id + '/', postData)
  }

  putUser(userForm: any, id: number) {
    let postData = new FormData();
    postData.append("username", userForm.username);
    postData.append("email", userForm.email);
    postData.append("password", userForm.new_password);

    return this.http.put('http://localhost:5000/api/v1/users/' + id + '/', postData)
  }

  deleteProduct(id: number) {
    return this.http.delete('http://localhost:5000/api/v1/products/' + id + '/')
  }

  deleteReview(product_id: number, review_id: number) {
    return this.http.delete('http://localhost:5000/api/v1/products/' + product_id + '/reviews/' + review_id + '/')
  }

  deleteUser(id: number) {
    return this.http.delete('http://localhost:5000/api/v1/users/' + id + '/')
  }

  deleteLogin() {
    return this.http.delete('http://localhost:5000/api/v1/logout/')
  }

  checkAdmin() {
    return this.http.get('http://localhost:5000/api/v1/checkAdmin/')
  }

  checkLogged() {
    return this.http.get('http://localhost:5000/api/v1/checkLogged/')
  }

  checkUser(id: number) {
    return this.http.get('http://localhost:5000/api/v1/checkUser/' + id + '/')
  }
}