import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { HomeComponent } from './home/home.component';
import { NewUserComponent } from './new_user/new_user.component';
import { EditUserComponent } from './edit_user/edit_user.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EditProductComponent } from './edit_product/edit_product.component';
import { NewProductComponent } from './new_product/new_product.component';
import { NewReviewComponent } from './new_review/new_review.component';
import { ReviewComponent } from './review/review.component';
import { EditReviewComponent } from './edit_review/edit_review.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: 'products/new',
    component: NewProductComponent
  },
  {
    path: 'products/:id',
    component: ProductComponent
  },
  {
    path: 'products/:id/reviews/new',
    component: NewReviewComponent
  },
  {
    path: 'products/:product_id/reviews/:review_id',
    component: ReviewComponent
  },
  {
    path: 'products/:product_id/reviews/:review_id/edit',
    component: EditReviewComponent
  },
  {
    path: 'products/:id/edit',
    component: EditProductComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'users/:id',
    component: UserComponent
  },
  {
    path: 'users/new',
    component: NewUserComponent
  },
  {
    path: 'users/:id/edit',
    component: EditUserComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
