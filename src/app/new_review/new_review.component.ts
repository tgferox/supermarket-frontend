import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-new_review',
  templateUrl: './new_review.component.html',
  styleUrls: ['./new_review.component.css']
})

export class NewReviewComponent implements OnInit {

  reviewForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.reviewForm = this.formBuilder.group({
      comment: ['', Validators.required],
      rating: ['', Validators.required]
    });
  }
    
  isInvalid(control: any) {
    return this.reviewForm.controls[control].invalid && this.reviewForm.controls[control].touched;
  }

  isUntouched() {
    return this.reviewForm.controls.comment.pristine || this.reviewForm.controls.rating.pristine;
    }

  isIncomplete() {
  return this.isInvalid('comment') || this.isInvalid('rating') || this.isUntouched();
  }

  onSubmit(){
    this.webService.postReview(this.reviewForm.value, this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        let uri = Object.values(data)
        this.router.navigate([uri[0]])
      },
      error: error => {
        this.review_message = 'Error: ' + error.error["error"]
      }
    });
  }

 
  review_message: any = "";
}

