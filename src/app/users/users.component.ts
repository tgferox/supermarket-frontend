import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  constructor(public webService: WebService) {}

  ngOnInit() {
    this.webService.getUsers(this.page, this.page_size)
    .subscribe({
      next: data => {
        this.user_list = data
      }
    });
  }

  user_list: any;
  page: number = 1;
  page_size: number = 10;
}

