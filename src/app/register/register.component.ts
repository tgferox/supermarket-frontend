import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  registerForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required, Validators.email],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required],
    });
  }
    
  isInvalid(control: any) {
    return this.registerForm.controls[control].invalid && this.registerForm.controls[control].touched;
  }

  isUntouched() {
    return this.registerForm.controls.username.pristine || this.registerForm.controls.email.pristine || this.registerForm.controls.password.pristine || this.registerForm.controls.password_confirm.pristine;
    }

  isIncomplete() {
  return this.isInvalid('username') || this.isInvalid('email') || this.isInvalid('password') || this.isInvalid('password_confirm') || this.isUntouched();
  }

  onSubmit(){
    this.webService.register(this.registerForm.value)
    .subscribe({
      next: data => {
        let token = Object.values(data)
        localStorage.setItem('token', token[0])
        this.router.navigate([''])
      },
      error: error => {
        this.register_message = 'Error: ' + error.error["error"]
      }
    });
  }

 
  register_message: any = "";
}

