import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit_user',
  templateUrl: './edit_user.component.html',
  styleUrls: ['./edit_user.component.css']
})

export class EditUserComponent implements OnInit {

  userForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required, Validators.email],
      new_password: ['', Validators.required]
    });
    this.webService.getUser(this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        let dataValues = data
        this.user = dataValues
      },
      error: err=> {
        this.router.navigate([''])
      }
    })
  }
    
  isInvalid(control: any) {
    return this.userForm.controls[control].invalid && this.userForm.controls[control].touched;
  }

  isUntouched() {
    return this.userForm.controls.new_password.pristine;
  }

  isIncomplete() {
    return this.isInvalid('username') || this.isInvalid('email') || this.isInvalid('new_password') || this.isUntouched();
  }

  onSubmit(){
    this.webService.putUser(this.userForm.value, this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        this.router.navigate(['/users/', {id: this.route.snapshot.params['id']}])
      },
      error: error => {
        this.user_message = 'Error: ' + error.error["error"]
      }
    });
  }

  onDelete(){
    this.webService.deleteUser(this.route.snapshot.params['id'])
    .subscribe({
      next: data=>{
        this.router.navigate([''])
      },
      error: error=> {
        this.delete_message = 'Error: ' + error.error["error"]
      }
    });
  }

  user: any;
  user_message: any = "";
  delete_message: any="";
}

