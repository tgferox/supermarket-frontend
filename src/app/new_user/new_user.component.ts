import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new_user',
  templateUrl: './new_user.component.html',
  styleUrls: ['./new_user.component.css']
})

export class NewUserComponent implements OnInit {

  userForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required, Validators.email],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required],
      is_admin: ['', Validators.required]
    });
  }
    
  isInvalid(control: any) {
    return this.userForm.controls[control].invalid && this.userForm.controls[control].touched;
  }

  isUntouched() {
    return this.userForm.controls.username.pristine || this.userForm.controls.email.pristine || this.userForm.controls.password.pristine || this.userForm.controls.password_confirm.pristine || this.userForm.controls.is_admin.pristine;
    }

  isIncomplete() {
  return this.isInvalid('username') || this.isInvalid('email') || this.isInvalid('password') || this.isInvalid('password_confirm') || this.isInvalid('is_admin') || this.isUntouched();
  }

  onSubmit(){
    this.webService.postUser(this.userForm.value)
    .subscribe({
      next: data => {
        this.router.navigate([''])
      },
      error: error => {
        this.user_message = 'Error: ' + error.error["error"]
      }
    });
  }

 
  user_message: any = "";
}

