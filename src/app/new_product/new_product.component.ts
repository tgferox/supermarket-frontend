import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new_product',
  templateUrl: './new_product.component.html',
  styleUrls: ['./new_product.component.css']
})

export class NewProductComponent implements OnInit {

  productForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      type: ['', Validators.required]
    }),
    this.webService.checkAdmin()
    .subscribe({
      next: data => {  
      },
      error: err=> {
        this.router.navigate([''])
      }
    })
  }
    
  isInvalid(control: any) {
    return this.productForm.controls[control].invalid && this.productForm.controls[control].touched;
  }

  isUntouched() {
    return this.productForm.controls.title.pristine || this.productForm.controls.description.pristine || this.productForm.controls.price.pristine || this.productForm.controls.type.pristine;
    }

  isIncomplete() {
  return this.isInvalid('title') || this.isInvalid('description') || this.isInvalid('price') || this.isInvalid('type') || this.isUntouched();
  }

  onSubmit(){
    this.webService.postProduct(this.productForm.value)
    .subscribe({
      next: data => {
        let uri = Object.values(data)
        this.router.navigate([uri[0]])
      },
      error: error => {
        this.product_message = 'Error: ' + error.error["error"]
      }
    });
  }

 
  product_message: any = "";
}

