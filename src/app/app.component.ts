import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { WebService } from './web.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'supermarket-frontend';

  constructor(private webService: WebService, private router: Router)
  {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        console.log("got here");
        this.webService.checkLogged()
        .subscribe({
          next: data => {  
            this.is_logged_in = true;
          },
          error: err=> {
            this.is_logged_in = false;
          }
        })
      }
    });

  
}
onLogout(){
  localStorage.removeItem('token');
  location.reload();
}

is_logged_in: any;
}
