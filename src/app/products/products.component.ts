import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

  constructor(public webService: WebService) {}

  ngOnInit() {
    this.webService.getProducts(this.page, this.page_size)
    .subscribe({
      next: data => {
        this.product_list = data
      }
    });
  }

  product_list: any;
  page: number = 1;
  page_size: number = 10;
}

