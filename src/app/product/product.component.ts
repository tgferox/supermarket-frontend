import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {

  constructor(private webService: WebService, private route: ActivatedRoute) {}
  ngOnInit() {
    this.webService.getProduct(this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        this.product = data
      }
    });
    this.webService.getReviews(this.page_num, this.page_size, this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        this.reviews = data
      }
    });
  }
   
  product: any;
  reviews: any;
  page_num = 1;
  page_size = 10;
}
