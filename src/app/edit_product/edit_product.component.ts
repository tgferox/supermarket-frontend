import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit_product',
  templateUrl: './edit_product.component.html',
  styleUrls: ['./edit_product.component.css']
})

export class EditProductComponent implements OnInit {

  productForm: any;

  constructor(private webService: WebService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      type: ['', Validators.required]
    }),
    this.webService.getProduct(this.route.snapshot.params['id'])
    .subscribe({
      next: data =>{
        let dataValues = data
        this.product = dataValues
        console.log(this.product)
        console.log(this.product["title"])
        this.webService.checkAdmin()
        .subscribe({
          next: data => {  
          },
          error: err=> {
            this.router.navigate([''])
          }
        })
      },
      error: error =>{
        this.router.navigate([''])
      }
    });
    
  }
    
  isInvalid(control: any) {
    return this.productForm.controls[control].invalid && this.productForm.controls[control].touched;
  }

  isIncomplete() {
    return this.isInvalid('title') || this.isInvalid('description') || this.isInvalid('price') || this.isInvalid('type');
  }

  onSubmit(){
    this.webService.putProduct(this.productForm.value, this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        this.router.navigate(['/products/', {id: this.route.snapshot.params['id']}])
      },
      error: error => {
        this.product_message = 'Error: ' + error.error["error"]
      }
    });
  }

  onDelete(){
    this.webService.deleteProduct(this.route.snapshot.params['id'])
    .subscribe({
      next: data=>{
        this.router.navigate([''])
      },
      error: error=> {
        this.delete_message = 'Error: ' + error.error["error"]
      }
    });
  }

  product: any = "";
  product_message: any = "";
  delete_message: any="";
}

