import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

  constructor(private webService: WebService, private route: ActivatedRoute) {}
  ngOnInit() {
    this.webService.getUser(this.route.snapshot.params['id'])
    .subscribe({
      next: data => {
        this.user = data
      }
    });
  }
   
  user: any;
  page_num = 1;
  page_size = 10;
}
