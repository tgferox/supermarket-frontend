import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})

export class ReviewComponent implements OnInit {

  constructor(private webService: WebService, private route: ActivatedRoute, private router: Router) {}
  ngOnInit() {
    this.product_id = this.route.snapshot.params['product_id']
    this.webService.getReview(this.route.snapshot.params['product_id'], this.route.snapshot.params['review_id'])
    .subscribe({
      next: data =>{
        let dataValues = data;
        this.review = dataValues;
      },
      error: error =>{
        this.router.navigate([''])
      }
    });
  }
  review: any;
  product_id: any
}
